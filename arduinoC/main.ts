
//% color="#FF8C00" iconWidth=50 iconHeight=40
namespace AS7341{
    //% block="VisibleSpectrumSensor Read channel value set Gain value" blockType="command"

    export function  AS7341InitSDR(parameter: any, block: any) {

        Generator.addInclude('AS7341', '#include "DFRobot_AS7341.h"');
        Generator.addObject("AS7341object2", "DFRobot_AS7341", "as7341;");
        Generator.addSetup("AS7341Setup1", ` while (as7341.begin() != 0) ;`);
        

        //Generator.addSetup("AS7341Setup2", "as7341.setAtime(29);");
        //Generator.addSetup("AS7341Setup3", "as7341.setAstep(599);");
        //Generator.addSetup("AS7341Setup4", `as7341.setAGAIN(${sdr});`);
    }

    
//% block="VisibleSpectrumSensor  enableLed [LED]" blockType="command"
//% LED.shadow="dropdown"   LED.options="LED" 
  export function  AS7341InitLED(parameter: any, block: any) {
    let led=parameter.LED.code;
    if(`${led}`==="ON"){
      Generator.addCode("as7341.enableLed(true);");
      Generator.addCode(`as7341.controlLed(1);`);
    }
    else if(`${led}` === "OFF"){
      Generator.addCode("as7341.enableLed(true);");
 }
}
 
    //% block="VisibleSpectrumSensor  [MODE]  " blockType="command"
    //% MODE.shadow="dropdown"   MODE.options="MODE"
    
    export function  AS7341InitMODEO(parameter: any, block: any) {
       let mode=parameter.MODE.code;
   
  
       if(`${mode}`==="1"){
        Generator.addCode(`DFRobot_AS7341::sModeOneData_t data1;`);
        Generator.addCode('as7341.startMeasure(as7341.eF1F4ClearNIR);');
        Generator.addCode(`data1 = as7341.readSpectralDataOne();`);
        
    }
    else if(`${mode}` === "2"){
    
        Generator.addCode(`DFRobot_AS7341::sModeTwoData_t data2;`);
        Generator.addCode('as7341.startMeasure(as7341.eF5F8ClearNIR);');
        Generator.addCode(`data2 = as7341.readSpectralDataTwo();`);
        
    }
  }

//    //% block="VisibleSpectrumSensor  MODE 1  passageway  [PSWO]  " blockType="reporter"
//       //% PSWO.shadow="dropdown"   PSWO.options="PSWO" 
//     export function  AS7341InitPSWO(parameter: any, block: any) {
//        let pswo=parameter.PSWO.code;
//        Generator.addCode([`${pswo}`,Generator.ORDER_UNARY_POSTFIX]);
 

// }


// //% block="VisibleSpectrumSensor  MODE 2" blockType="command"
// export function  AS7341InitMODET(parameter: any, block: any) {
 
//     Generator.addCode('DFRobot_AS7341::sModeTwoData_t data2;');
//     Generator.addCode('as7341.startMeasure(as7341.eF5F8ClearNIR);');
//     Generator.addCode('data2 = as7341.readSpectralDataTwo();');
 
    
// }

//% block="VisibleSpectrumSensor   passageway  [PSW]  " blockType="reporter"
//% PSW.shadow="dropdown"   PSW.options="PSW" 
export function  AS7341InitPSW(parameter: any, block: any) {
   let psw=parameter.PSW.code;
   Generator.addCode([`${psw}`,Generator.ORDER_UNARY_POSTFIX]);

}

 
// //% block="VisibleSpectrumSensor  enableLed OFF" blockType="command"

// export function  AS7341InitOFF(parameter: any, block: any) {

//     Generator.addCode("as7341.enableLed(true);");

//  }
  

//  //% block="VisibleSpectrumSensor Measure the flicker frequency of light source set Gain value[SDR]" blockType="command"
//     //% SDR.shadow="dropdownRound"   SDR.options="SDR"   

//     export function  AS7341InitSDRP(parameter: any, block: any) {
//         let sdr=parameter.SDR.code;
   
//         Generator.addInclude('AS7341', '#include "DFRobot_AS7341.h"');
//         Generator.addObject("AS7341object2", "DFRobot_AS7341", "as7341;");
//         Generator.addSetup("AS7341Setup1", ` while (as7341.begin() != 0) ;`);
//         Generator.addSetup("AS7341Setup2", "as7341.setAtime(100);");
//         Generator.addSetup("AS7341Setup3", "as7341.setAstep(999);");
//         Generator.addSetup("AS7341Setup4", `as7341.setAGAIN(${sdr});`);
        
//     }

 
    //% block="VisibleSpectrumSensor  freq" blockType="reporter"
    export function  AS7341InitFREQ(parameter: any, block: any) {
        Generator.addCode(['as7341.readFlickerData()',Generator.ORDER_UNARY_POSTFIX]);   
    } 

}
