# DFRobot_AS7341  光谱传感器  

![](./arduinoC/_images/featured.png)  


您对颜色了解多少？您想了解真正的颜色吗？眼睛可能会欺骗您，但是传感器不会。AS7341可见光传感器可以告诉您最真实的颜色。  

## 目录

* [相关链接](#相关链接)
* [描述](#描述)
* [积木列表](#积木列表)
* [示例程序](#示例程序)
* [许可证](#许可证)
* [支持列表](#支持列表)
* [更新记录](#更新记录)  


## 相关链接  

* 本项目加载链接```https://gitee.com/jeff111203/ext-as57341.```    


* 用户库教程链接: ```https://mindplus.dfrobot.com.cn/extensions-user```  

* 购买此产品: [商城](https://www.dfrobot.com.cn/goods-2927.html).  
  
## 描述  

AS7341可见光传感器采用业内知名的ams公司推出的新一代AS7341光谱传感IC。该传感器有8个可见光通道、1个闪烁通道、1个NIR通道和1个未加滤光片的通道。该传感器拥有6个独立的16位ADC通道，可以并行的处理数据。该传感器板载了两颗高亮LED，可在暗光环境下进行补光。 

## 积木列表  

![](./arduinoC/_images/blocks.png)  
  
## 示例程序  
**读取光谱传感器对应通道数值：**  

![](./arduinoC/_images/example1.png)  

**读取光源闪烁频率：**  

![](./arduinoC/_images/example2.png)  

  
## 许可证  

MIT  

## 支持列表  


主板型号                | 实时模式    | ArduinoC   | MicroPython    | 备注
------------------ | :----------: | :----------: | :---------: | -----
arduino uno        |             |        √      |             | 
leonardo        |             |        √      |             | 
mega2560        |             |        √      |             |
esp32        |             |        √      |             | 

## 更新日志
* V0.0.1  基础功能完成
